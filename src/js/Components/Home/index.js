import React from "react";
import BreadCrumbs from "../Common/Breadcrumbs";
import Input from "../Common/Form/TextField";
import Radio from "../Common/Form/Input";
import Button from "../Common/Form/Button";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";
function Home() {
  return (
    <>
      <BreadCrumbs />
      <Form className="d-flex flex-wrap w-100 p-5">
        <Input
          id="text"
          label="Test"
          type="text"
          placeholder="Testing"
          defaultValue="Testing"
          helperText="Helper Testing"
          className="p-1"
        />
        <Input
          id="text"
          label="Test Textarea"
          type="textarea"
          placeholder="Testing"
          defaultValue="Testing"
          helperText="Helper Testing"
          className="p-1"
          row={10}
        />
        <Radio
          inline={true}
          disabled={false}
          label="Test Radio"
          type="radio"
          id="Test"
        />
        <Radio
          inline={true}
          disabled={false}
          label="Test Checkbox"
          type="checkbox"
          id="Test"
        />
        <Button
          variant="primary"
          size="lg"
          value="Test"
          block={true}
          disabled={true}
          onClick={() => alert("Clicked")}
          className="mt-3"
        />
        <Button
          variant="primary"
          size="lg"
          value="Test"
          block={false}
          disabled={false}
          onClick={() => alert("Clicked")}
          className="mt-3"
        />
        <Button
          variant="outline-danger"
          size="md"
          value="Test"
          block={false}
          disabled={false}
          onClick={() => alert("Clicked")}
          className="mt-3 ml-3"
        />
        <Button
          variant="outline-dark"
          size="sm"
          value="Test"
          block={false}
          disabled={false}
          onClick={() => alert("Clicked")}
          className="mt-3 ml-3"
        />

        <div className="w-100">
          <Link
            to="/Home/Test"
            className="bg-primary w-25 mt-3 justify-content-center p-2 text-white d-flex align-items-center"
          >
            TO ANOTHE ROUTE
          </Link>
        </div>
        <div className="w-100">
          <Link
            to="/Home/Test/Deep"
            className="bg-primary w-25 mt-3 justify-content-center p-2 text-white d-flex align-items-center"
          >
            TO ANOTHE ROUTE DEEP
          </Link>
        </div>
      </Form>
    </>
  );
}
export default Home;
