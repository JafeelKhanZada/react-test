import React from "react";
import { useLocation } from "react-router";
import { Breadcrumb } from "react-bootstrap";
import { Link } from "react-router-dom";

function RouterBreadcrumbs(props) {
  const location = useLocation();
  const pathnames = location.pathname.split("/").filter((x) => x);
  return (
    <Breadcrumb>
      {pathnames.map((value, index) => {
        const last = index === pathnames.length - 1;
        const to = `/${pathnames.slice(0, index + 1).join("/")}`;
        return last ? (
          <p className="p-0 m-0" key={index}>
            {value}
          </p>
        ) : (
          <>
            <Link to={to} key={index}>
              {`${value}`}
            </Link>
            <p key={index} className="my-0 mx-3 p-0">{`>`}</p>
          </>
        );
      })}
    </Breadcrumb>
  );
}
export default RouterBreadcrumbs;
