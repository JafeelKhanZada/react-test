import React from "react";
import { Form } from "react-bootstrap";
import PropTypes from "prop-types";
function Input(props) {
  const {
    id,
    inline,
    disabled,
    label,
    type,
    onChange,
    defaultValue,
    value,
    className,
  } = props;
  return (
    <Form.Check
      inline={inline}
      disabled={disabled}
      label={label}
      type={type}
      id={id}
      onChange={onChange}
      defaultChecked={defaultValue}
      checked={value}
      className={className}
    />
  );
}
Input.propTypes = {
  id: PropTypes.string,
  inline: PropTypes.bool,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.bool,
  defaultValue: PropTypes.bool,
  className: PropTypes.string,
};
export default Input;
