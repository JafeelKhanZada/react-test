import React from "react";
import { Form } from "react-bootstrap";
import PropTypes from "prop-types";
function TextField(props) {
  const {
    id,
    label,
    type,
    placeholder,
    className,
    value,
    onChange,
    defaultValue,
    helperText,
    row,
  } = props;
  return (
    <Form.Group className="w-100" controlId={id}>
      <Form.Label>{label}</Form.Label>
      <Form.Control
        className={className}
        type={type}
        placeholder={placeholder}
        defaultValue={defaultValue}
        value={value}
        onChange={onChange}
        rows={row}
        {...(type === "textarea" ? { as: "textarea" } : {})}
      />
      <Form.Text className="text-muted">{helperText}</Form.Text>
    </Form.Group>
  );
}
TextField.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  className: PropTypes.string,
  value: PropTypes.string || PropTypes.number,
  onChange: PropTypes.func,
  helperText: PropTypes.string,
  defaultValue: PropTypes.string,
  row: PropTypes.number,
};
export default TextField;
