import React from "react";
import { Button as BTN } from "react-bootstrap";
import PropTypes from "prop-types";
function Button(props) {
  const {
    variant,
    type,
    value,
    size,
    block,
    disabled,
    onClick,
    className,
    href,
  } = props;
  return (
    <BTN
      variant={variant}
      href={href}
      type={type}
      size={size}
      active={!disabled}
      block={block}
      disabled={disabled}
      onClick={onClick}
      className={className}
    >
      {value}
    </BTN>
  );
}
Button.propTypes = {
  variant: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  size: PropTypes.string,
  block: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  className: PropTypes.string,
  href: PropTypes.string,
};
export default Button;
