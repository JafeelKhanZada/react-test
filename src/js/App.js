import React from "react";
import Router from "./Util/App.router";
import "bootstrap/dist/css/bootstrap.min.css";
function App() {
  return (
    <>
      <Router />
    </>
  );
}

export default App;
