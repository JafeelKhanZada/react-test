import React from "react";
import Loadable from "react-loadable";
import { BrowserRouter, Route, Switch } from "react-router-dom";
const AsynHome = Loadable({
  loader: () => import("../Components/Home"),
  loading: () => <div>Loading......</div>,
});
const AsyncTest = Loadable({
  loader: () => import("../Components/Test"),
  loading: () => <div>Loading......</div>,
});
const AsyncTest2 = Loadable({
  loader: () => import("../Components/Test2"),
  loading: () => <div>Loading......</div>,
});
const Router = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/Home" exact component={AsynHome} />
        <Route
          path="/Home/Test"
          exact
          render={(props) => <AsyncTest {...props} />}
        />
        <Route
          path="/Home/Test/Deep"
          exact
          render={(props) => <AsyncTest2 {...props} />}
        />
      </Switch>
    </BrowserRouter>
  );
};
export default Router;
